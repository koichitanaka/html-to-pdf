class PdfController < ApplicationController
  def pdf
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "title-#{Time.now.to_date.to_s}",
               :template => 'pdf/pdf.html.erb',
               :layout => 'pdf.html',
               :page_size => 'A4',
               :dpi => '400',
               :show_as_html => params[:debug].present?
      end
    end
  end
end
